PREFIX := arm-none-eabi-
CC := $(PREFIX)gcc
OBJCOPY := $(PREFIX)objcopy

SRCDIR := src
OBJDIR := obj
INCDIR := inc

INCFLAGS = -Idrivers/Inc -Iinc -ICMSIS/Include
LDFLAGS = -static -TSTM32G0B1RE.ld
LDFLAGS += -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group

CFLAGS = -std=c11 -g3 -Os -fno-common -ffunction-sections -fdata-sections

# Specific to the board
CFLAGS += -mcpu=cortex-m0plus
CFLAGS += -DSTM32G0B1xx

# Add Qubo embedded source files here
SRCS := main.c config.c stm32g0xx_it.c callbacks.c

# --------------- Should not need to edit anything below here ---------- #
SRCS += drivers/*.c
SRCS := $(addprefix $(SRCDIR)/, $(SRCS))

OBJS := $(patsubst src/%.c,obj/%.o,$(wildcard $(SRCS)))

all: dirs image.bin

dirs:
	mkdir -p obj obj/drivers

%.bin: %.elf
	$(OBJCOPY) -Obinary $< $@

rx.elf: src/drivers/startup_stm32g0b1xx.s $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS) $(LIBS)

tx.elf: src/drivers/startup_stm32g0b1xx.s $(OBJS)
	$(CC) $^ -o $@  $(CFLAGS) $(LDFLAGS) $(LIBS)

# image.bin: image.elf
# 	$(OBJCOPY) -Obinary image.elf image.bin

image.elf: src/drivers/startup_stm32g0b1xx.s $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS) $(LIBS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(INCFLAGS) $(ARGS) $(CFLAGS) -c $< -o $@

openocd:
	openocd -f interface/stlink.cfg -f target/stm32g0x.cfg

rx_flash:
	st-flash --flash=512k --freq=4M --format=binary --reset \
	--connect-under-reset write rx.bin 0x8000000

tx_flash:
	st-flash --flash=512k --freq=4M --format=binary --reset \
	--connect-under-reset write tx.bin 0x8000000

# flash:
# 	st-flash --flash=512k --freq=4M --format=binary --reset \
# 	--connect-under-reset write image.bin 0x8000000

clean:
	- rm -r image.elf image.bin obj

.PHONY: all dirs clean openocd flash

