/* 2024 Robotics @ Maryland
   Alexander Yelovich <alexyel84@gmail.com>

   Defines interrupt functions used by our project.
*/

#include "stm32g0xx_hal.h"

void SysTick_Handler(void) {
    HAL_IncTick();
}


void TIM16_FDCAN_IT0_IRQHandler(void) {
    extern FDCAN_HandleTypeDef hcan;
    HAL_FDCAN_IRQHandler(&hcan);
}

