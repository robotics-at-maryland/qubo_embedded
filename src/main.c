// main.c: Blink an led

#include "stm32g0xx_hal.h"

#include "config.h"
#include "qubodefs.h"

FDCAN_HandleTypeDef hcan;

int main(void) {
    // this is the handle for the CAN instance on this device (there is only one)
    //FDCAN_HandleTypeDef hcan;    
    
    HAL_Init();
    config_clock();
    config_led();
    config_fdcan(&hcan);

    HAL_NVIC_SetPriority(SysTick_IRQn, 1, 0);

    HAL_Delay(1000);

    
    FDCAN_TxHeaderTypeDef thruster_tx = {
        .Identifier = QUBO_CAN_THRUSTER_CTRL_ALL,
        .IdType = FDCAN_STANDARD_ID,
        .TxFrameType = FDCAN_DATA_FRAME,
        .DataLength = FDCAN_DLC_BYTES_8,
        .ErrorStateIndicator = FDCAN_ESI_ACTIVE,
        .BitRateSwitch = FDCAN_BRS_OFF,
        .FDFormat = FDCAN_FRAME_CLASSIC,
        .TxEventFifoControl = FDCAN_STORE_TX_EVENTS,
        .MessageMarker = 0
    };
    uint8_t thruster_tx_buf[8] = "AAAAAAA";

#if defined(RAM_CAN_LOOPBACK)
    HAL_FDCAN_AddMessageToTxFifoQ(&hcan, &thruster_tx, thruster_tx_buf);
#endif

    FDCAN_TxEventFifoTypeDef event = {0};
    HAL_FDCAN_GetTxEvent(&hcan, &event);
    
    //HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
    
    for (;;)
        
        ;
    
    /* if (config_fdcan(&hcan) != HAL_OK) */
    /*     HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6); */

    /* if (HAL_RCC_GetSysClockFreq() == 170000000U) */
    /*     HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6); */

    /* for (;;) { */
    /*     HAL_Delay(1000); */
    /*     HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6); */
    /* } */
}
