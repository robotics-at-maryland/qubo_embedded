/* 2024 Robotics @ Maryland
   Alexander Yelovich <alexyel84@gmail.com>

   Defines callback functions used by our project.
*/

#include "stm32g0xx_hal.h"

#include "qubodefs.h"

#define THRUSTER_DATA 1
#define VOLTAGE_REPORT 2
#define ALERT 3

#define MIN_THRUST (uint16_t) 1250
#define MAX_THRUST (uint16_t) 1750

void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs) {
    /* extern FDCAN_RxHeaderTypeDef thruster_rx; */
    /* extern uint8_t  */
    FDCAN_RxHeaderTypeDef thruster_rx;
    uint8_t thruster_rx_buf[8];

    
    if (RxFifo0ITs & FDCAN_IT_RX_FIFO0_NEW_MESSAGE) { // we got a message
        if (HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &thruster_rx, thruster_rx_buf) != HAL_OK) {
            // something went wrong 
            // TODO Add some indication of this
            return;
        };
        // Load message into message RAM
        HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &thruster_rx, thruster_rx_buf);
        // Toggle LED (message received)
        HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);

        // do a thruster receipt function here, after which these variables
        // are cleared, which is fine since we are just going to sit around and
        // wait until we get another message

        // TODO: make this more robust by maybe using DMA or some way to buffer
        // messages until the thrusters application can do something about this
        
        // Only attempt to parse the mesage if it's properly formatted
        if (thruster_rx.Identifier == QUBO_CAN_THRUSTER_CTRL_ALL && thruster_rx.IdType == FDCAN_STANDARD_ID && thruster_rx.DataLength == FDCAN_DLC_BYTES_8) { 
            // Buffer protocol:
            // Byte 0: Packet Type (Thruster, Voltage/Current, Alert)
            switch (thruster_rx_buf[0]) {
                case THRUSTER_DATA:
                    // Bytes 1-2 for thruster data (1250 to 1750) using little-endian
                    uint16_t thruster = ((uint16_t) (thruster_rx_buf[1])) ^ (((uint16_t) (thruster_rx_buf[2])) << 8);  
                    // Clamp the value to the interval [1250,1750]
                    thruster = thruster < MIN_THRUST ? MIN_THRUST : (thruster > MAX_THRUST ? MAX_THRUST : thruster);
                    break;
                case VOLTAGE_REPORT:
                    // voltage/current report
                    // Byte 1: bus
                    // Bytes 2-4: current
                    // Bytes: 5-7: voltage
                    // TODO Add alert messages for this for bad readings + Add metrics logging
                    uint8_t bus = thruster_rx_buf[1];
                    uint32_t current = ((uint32_t) (thruster_rx_buf[2])) ^ (((uint32_t) (thruster_rx_buf[3])) << 8) ^ (((uint32_t) (thruster_rx_buf[4])) << 16);          
                    uint32_t voltage = ((uint32_t) (thruster_rx_buf[5])) ^ (((uint32_t) (thruster_rx_buf[6])) << 8) ^ (((uint32_t) (thruster_rx_buf[7])) << 16);
                    break;
                case ALERT:
                    // alert message detection
                    // Bytes 1-2: alert status/message/code
                    uint16_t message = ((uint16_t) (thruster_rx_buf[1])) ^ (((uint16_t) (thruster_rx_buf[2])) << 8);
                    // TODO Add parsing for the various desired/required error messages
                    break;
            }
        }
        // Clear (zero) the 8-byte buffer
        for (uint32_t i = 0; i < FDCAN_DLC_BYTES_8; ++i) {
            thruster_rx_buf[i] = 0;
        }
    }
}
