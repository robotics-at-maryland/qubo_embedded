/* 2024 Robotics @ Maryland
   Alexander Yelovich <alexyel84@gmail.com>

   All the configuration code for the STM32 peripherals goes here. This file
   currently contains configurations for the following:
   - The System Clock
   - Blinking the user LED
   - CAN Bus
*/

#include "stm32g0xx_hal.h"
#include "config.h"
#include "qubodefs.h"

void config_clock(void) {
    RCC_OscInitTypeDef osc_config = {0};
    RCC_PLLInitTypeDef pll_config = {0};
    RCC_ClkInitTypeDef clk_config = {0};

    // PLL Configuration
    // SysClock at 64 MHz
    // PLLQ Clock at 25.6 MHz
    pll_config.PLLState = RCC_PLL_ON;
    pll_config.PLLSource = RCC_PLLSOURCE_HSI;
    pll_config.PLLM = RCC_PLLM_DIV2;
    pll_config.PLLN = 16;
    pll_config.PLLP = RCC_PLLP_DIV16;
    pll_config.PLLQ = RCC_PLLQ_DIV5;
    pll_config.PLLR = RCC_PLLR_DIV2;

    osc_config.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    osc_config.HSIState = RCC_HSICALIBRATION_DEFAULT; // do not choose BYPASS because we have
                                      // a crystal on board
    osc_config.HSIDiv = RCC_HSI_DIV1;
    osc_config.PLL = pll_config;

    // PLL Configuration
    // SysClock at 64 MHz
    // PLLQ Clock at 25.6 MHz
    clk_config.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK
                         | RCC_CLOCKTYPE_PCLK1;
    clk_config.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    clk_config.AHBCLKDivider = RCC_SYSCLK_DIV1;
    clk_config.APB1CLKDivider = RCC_HCLK_DIV1;

    volatile int oscout = HAL_RCC_OscConfig(&osc_config);
    volatile int clkout = HAL_RCC_ClockConfig(&clk_config, FLASH_LATENCY_2);
    SystemCoreClockUpdate();
    
    // Disable the PLLP since it is not used
    __HAL_RCC_PLLCLKOUT_DISABLE(RCC_PLLPCLK);
}

void config_led(void) {
    GPIO_InitTypeDef led_config = {
        .Pin = GPIO_PIN_5,
        .Mode = GPIO_MODE_OUTPUT_PP,
        .Pull = GPIO_NOPULL,
        .Speed = GPIO_SPEED_FREQ_LOW,
        .Alternate = 0 // we don't use this part
    };

    __HAL_RCC_GPIOA_CLK_ENABLE();
    HAL_GPIO_Init(GPIOA, &led_config);
}

void HAL_FDCAN_MspInit(FDCAN_HandleTypeDef *hfdcan) {
    GPIO_InitTypeDef gpio_config = {0};

    // first the RX pin
    gpio_config = (GPIO_InitTypeDef) {
        .Pin = GPIO_PIN_4,
        .Mode = GPIO_MODE_OUTPUT_PP,
        .Pull = GPIO_NOPULL,
        .Speed = GPIO_SPEED_FREQ_VERY_HIGH,
        .Alternate = GPIO_AF3_FDCAN1
    };

    __HAL_RCC_GPIOC_CLK_ENABLE();
    HAL_GPIO_Init(GPIOC, &gpio_config);

    // now the TX pin
    gpio_config = (GPIO_InitTypeDef) {
        .Pin = GPIO_PIN_5,
        .Mode = GPIO_MODE_OUTPUT_PP,
        .Pull = GPIO_NOPULL,
        .Speed = GPIO_SPEED_FREQ_VERY_HIGH,
        .Alternate = GPIO_AF3_FDCAN1
    };

    //__HAL_RCC_GPIOB_CLK_ENABLE();
    HAL_GPIO_Init(GPIOC, &gpio_config);

    gpio_config = (GPIO_InitTypeDef) {
        .Pin = GPIO_PIN_8,
        .Mode = GPIO_MODE_OUTPUT_PP,
        .Pull = GPIO_NOPULL,
        .Speed = GPIO_SPEED_FREQ_LOW,
        .Alternate = 0
    };

    
    __HAL_RCC_GPIOD_CLK_ENABLE();
    HAL_GPIO_Init(GPIOD, &gpio_config);

    gpio_config = (GPIO_InitTypeDef) {
        .Pin = GPIO_PIN_9,
        .Mode = GPIO_MODE_OUTPUT_PP,
        .Pull = GPIO_NOPULL,
        .Speed = GPIO_SPEED_FREQ_LOW,
        .Alternate = 0
    };
    HAL_GPIO_Init(GPIOD, &gpio_config);

    HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_9);

    // configure the peripheral clock for FDCAN
    RCC_PeriphCLKInitTypeDef pclk_config;
    HAL_RCCEx_GetPeriphCLKConfig(&pclk_config);
    pclk_config.FdcanClockSelection = RCC_FDCANCLKSOURCE_PLL;
    HAL_RCCEx_PeriphCLKConfig(&pclk_config);

    __HAL_RCC_FDCAN_CLK_ENABLE();

    // enable the interrupts for FDCAN
    HAL_NVIC_SetPriority(TIM16_FDCAN_IT0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM16_FDCAN_IT0_IRQn);
}

void config_fdcan(FDCAN_HandleTypeDef *hcan) {
    hcan->Instance = FDCAN1;
    hcan->Init = (FDCAN_InitTypeDef) {
        .ClockDivider = FDCAN_CLOCK_DIV10,
        .FrameFormat = FDCAN_FRAME_CLASSIC,
#if defined(RAM_CAN_NORMAL)
        .Mode = FDCAN_MODE_NORMAL,
#elif defined(RAM_CAN_LOOPBACK)
        .Mode = FDCAN_MODE_EXTERNAL_LOOPBACK,
#endif
        // Nominal -> FD_CAN
        // Data -> CAN
        .AutoRetransmission = ENABLE,
        .TransmitPause = ENABLE,
        .ProtocolException = DISABLE,
        .NominalPrescaler = 32,
        .NominalSyncJumpWidth = 4,
        // Bit Timing -> SyncSeg + NominalTimeSeg1 + NominalTimeSeg2 
        // SyncSeg (fixed to 1)
        .NominalTimeSeg1 = 13,
        .NominalTimeSeg2 = 2,
        .DataPrescaler = 32,
        .DataSyncJumpWidth = 4,
        .DataTimeSeg1 = 13,
        .DataTimeSeg2 = 2,
        .StdFiltersNbr = 1,
        .ExtFiltersNbr = 0,
        .TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION
    };

    // this calls the FDCAN MspInit function
    HAL_FDCAN_Init(hcan);

    // Now configure the filters
    FDCAN_FilterTypeDef thruster_control_filter = {
        .IdType = FDCAN_STANDARD_ID,
        .FilterIndex = 0,
        .FilterType = FDCAN_FILTER_MASK,
        .FilterConfig = FDCAN_FILTER_TO_RXFIFO0,
        .FilterID1 = QUBO_CAN_THRUSTER_CTRL_BASE,
        .FilterID2 = QUBO_CAN_THRUSTER_CTRL_MASK
    };

    HAL_FDCAN_ConfigFilter(hcan, &thruster_control_filter);
    HAL_FDCAN_ConfigGlobalFilter(hcan, FDCAN_REJECT, FDCAN_REJECT,
                                 FDCAN_FILTER_REMOTE, FDCAN_REJECT_REMOTE);

    // we are good to start
    HAL_FDCAN_Start(hcan);
    HAL_FDCAN_ActivateNotification(hcan, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0);
}
