#ifndef CONFIG_H
#define CONFIG_H

#include "stm32g0xx_hal.h"

void config_clock(void);
void config_led(void);
void config_fdcan(FDCAN_HandleTypeDef *hcan);

#endif
