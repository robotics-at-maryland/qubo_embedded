/* 2024 Robotics @ Maryland
   Alexander Yelovich <alexyel84@gmail.com>

   Contains definitions that are common to all the boards on Qubo:
   the Jetson, embedded boards, and the sonar board.
*/

#ifndef QUBODEFS_H
#define QUBODEFS_H

#include <stdint.h>

#define QUBO_CAN_THRUSTER_CTRL_MASK ((uint32_t) 0x7FBU)
#define QUBO_CAN_THRUSTER_CTRL_BASE ((uint32_t) 0x004U)

// CAN thruster control message providing thrust values for all 8 thrusters
#define QUBO_CAN_THRUSTER_CTRL_ALL QUBO_CAN_THRUSTER_CTRL_BASE

#endif
