/* 2024 Robotics @ Maryland

   Contains common definitions for this project so that the programmer need not
   remember that to blink the led you need to use port C pin 6 for example.
*/

#ifndef COMMON_H
#define COMMON_H

#include "stm32g4xx_hal.h"

#define USER_LED_PORT GPIOC
#define USER_LED_PIN GPIO_PIN_6

#endif
